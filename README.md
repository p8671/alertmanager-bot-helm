# alertmanager-bot-helm

[![pipeline status](https://gitlab.com/msvechla/alertmanager-bot-helm/badges/master/pipeline.svg)](https://gitlab.com/msvechla/alertmanager-bot-helm/commits/master)

*A Helm Chart for deploying the [metalmatze/alertmanager-bot](https://github.com/metalmatze/alertmanager-bot) telegram bot to Kubernetes.*

## Quickstart

Create a new bot by talking to [@botfather](https://telegram.me/botfather) and take note of the bot token that will be presented during the process.
You will also need your personal user ID, which can be retrieved from [@userinfobot](https://telegram.me/userinfobot).

The bot can then be deployed via:

```bash
helm upgrade -i telegram alertmanager-bot/ \
  --set "telegram.token.raw=1234:myTokenFromBotfather" \
  --set "telegram.adminUserIDs[0]='idFromUserInfoBot'" \
  --set "alertmanager.url=http://prometheus-operator-alertmanager:9093"
```

## Pulling the Helm Chart

You can also use the experimental helm OCI registry feature to pull this chart:

```sh
export HELM_EXPERIMENTAL_OCI=1
helm chart pull registry.gitlab.com/msvechla/alertmanager-bot-helm:v0.1.0
helm chart export registry.gitlab.com/msvechla/alertmanager-bot-helm:v0.1.0
```

Alternatively simply check out this git repo locally.

## Chart Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| alertmanager.url | string | `""` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.repository | string | `"metalmatze/alertmanager-bot"` |  |
| image.tag | string | `"0.4.2"` |  |
| imagePullSecrets | list | `[]` |  |
| messageTemplate | string | `"{{ define \"telegram.default\" }}\n{{ range .Alerts }}\n{{ if eq .Status \"firing\"}}🔥 \u003cb\u003e{{ .Status | toUpper }}\u003c/b\u003e 🔥{{ else }}\u003cb\u003e{{ .Status | toUpper }}\u003c/b\u003e{{ end }}\n\u003cb\u003e{{ .Labels.alertname }}\u003c/b\u003e\n{{ .Annotations.message }}\n\u003cb\u003eDuration:\u003c/b\u003e {{ duration .StartsAt .EndsAt }}{{ if ne .Status \"firing\"}}\n\u003cb\u003eEnded:\u003c/b\u003e {{ .EndsAt | since }}{{ end }}\n{{ end }}\n{{ end }}\n"` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| persistence.enabled | bool | `false` |  |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| replicaCount | int | `1` |  |
| resources.requests.memory | string | `"15Mi"` |  |
| securityContext | object | `{}` |  |
| service.port | int | `80` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | `""` |  |
| telegram.adminUserIDs | list | `[]` |  |
| telegram.token.raw | string | `""` |  |
| tolerations | list | `[]` |  |
